package com.ollera.moxy.mvp.storage

actual class StateStorage {
    val map: MutableMap<String, String> = mutableMapOf()
    val storageMap: MutableMap<String, StateStorage> = mutableMapOf()

    actual fun getStateStorage(key: String): StateStorage? {
        return storageMap[key]
    }

    actual fun containsKey(key: String): Boolean {
        return map.containsKey(key)
    }

    actual fun getString(key: String): String {
        return map[key] ?: ""
    }

    actual fun putStateStorage(key: String, storage: StateStorage) {
        storageMap[key] = storage
    }

    actual fun putAll(storage: StateStorage?) {
        map.putAll(storage?.map ?: emptyMap())
    }

    actual fun putString(key: String, value: String) {
        map[key] = value
    }
}