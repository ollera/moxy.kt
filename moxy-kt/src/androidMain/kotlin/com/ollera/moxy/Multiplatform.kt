package com.ollera.moxy

import java.util.*

actual fun <T> createWeakSet(): MutableSet<T> {
    return Collections.newSetFromMap(WeakHashMap())
}

actual fun <K, T> createWeakMap(): MutableMap<K, T> {
    return WeakHashMap()
}