package com.ollera.moxy.mvp.storage

import android.os.Bundle

actual class StateStorage {

    val bundle: Bundle

    actual constructor() : this(Bundle())

    constructor(bundle: Bundle) {
        this.bundle = bundle
    }

    actual fun getStateStorage(key: String): StateStorage? {
        return StateStorage(bundle.getBundle(key) ?: Bundle())
    }

    actual fun containsKey(key: String): Boolean {
        return bundle.containsKey(key)
    }

    actual fun getString(key: String): String {
        return bundle.getString(key) ?: ""
    }

    actual fun putStateStorage(key: String, storage: StateStorage) {
        bundle.putBundle(key, storage.bundle)
    }

    actual fun putAll(storage: StateStorage?) {
        storage?.let { bundle.putAll(storage.bundle) }
    }

    actual fun putString(key: String, value: String) {
        bundle.putString(key, value)
    }
}