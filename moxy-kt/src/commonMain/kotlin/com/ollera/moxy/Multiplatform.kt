package com.ollera.moxy

expect fun <T> createWeakSet(): MutableSet<T>
expect fun <K, T> createWeakMap(): MutableMap<K, T>