package com.ollera.moxy.mvp.viewstate.strategy

import com.ollera.moxy.mvp.MvpView
import com.ollera.moxy.mvp.viewstate.ViewCommand

/**
 * Command will be added to end of commands queue.
 *
 * This strategy used by default.
 */
class AddToEndStrategy : StateStrategy {
    override fun <View : MvpView> beforeApply(currentState: MutableList<ViewCommand<View>>, incomingCommand: ViewCommand<View>) {
        currentState.add(incomingCommand)
    }

    override fun <View : MvpView> afterApply(currentState: MutableList<ViewCommand<View>>, incomingCommand: ViewCommand<View>) {
        // pass
    }
}
