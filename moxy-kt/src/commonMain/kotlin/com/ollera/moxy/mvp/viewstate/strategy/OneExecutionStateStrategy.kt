package com.ollera.moxy.mvp.viewstate.strategy

import com.ollera.moxy.mvp.MvpView
import com.ollera.moxy.mvp.viewstate.ViewCommand

/**
 * Command will be saved in commands queue. And this command will be removed after first execution.
 */

class OneExecutionStateStrategy : StateStrategy {
    override fun <View : MvpView> beforeApply(currentState: MutableList<ViewCommand<View>>, incomingCommand: ViewCommand<View>) {
        currentState.add(incomingCommand)
    }

    override fun <View : MvpView> afterApply(currentState: MutableList<ViewCommand<View>>, incomingCommand: ViewCommand<View>) {
        currentState.remove(incomingCommand)
    }
}
