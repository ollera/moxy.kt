package com.ollera.moxy.mvp.viewstate

import com.ollera.moxy.createWeakMap
import com.ollera.moxy.createWeakSet
import com.ollera.moxy.mvp.MvpView

@Suppress("MemberVisibilityCanBePrivate")
abstract class MvpViewState<View : MvpView> {
    protected var mViewCommands = ViewCommands<View>()
    protected var mViews: MutableSet<View> = createWeakSet()
    protected var mInRestoreState: MutableSet<View> = createWeakSet()
    protected var mViewStates: MutableMap<View, Set<ViewCommand<View>>> = createWeakMap()

    /**
     * @return views, attached to this view state instance
     */
    val views: Set<View>
        get() = mViews

    /**
     * Apply saved state to attached view
     *
     * @param view mvp view to restore state
     * @param currentState commands that was applied already
     */
    protected fun restoreState(view: View, currentState: Set<ViewCommand<View>>) {
        if (mViewCommands.isEmpty) {
            return
        }

        mViewCommands.reapply(view, currentState)
    }

    /**
     * Attach view to view state and apply saves state
     *
     * @param view attachment
     */
    fun attachView(view: View?) {
        if (view == null) {
            throw IllegalArgumentException("Mvp view must be not null")
        }

        val isViewAdded = mViews.add(view)

        if (!isViewAdded) {
            return
        }

        mInRestoreState.add(view)

        val currentState: Set<ViewCommand<View>> = mViewStates[view] ?: emptySet()

        restoreState(view, currentState)

        mViewStates.remove(view)

        mInRestoreState.remove(view)
    }

    /**
     *
     * Detach view from view state. After this moment view state save
     * commands via
     * [com.ollera.moxy.mvp.viewstate.strategy.StateStrategy.beforeApply].
     *
     * @param view target mvp view to detach
     */
    fun detachView(view: View) {
        mViews.remove(view)
        mInRestoreState.remove(view)

        val currentState = createWeakSet<ViewCommand<View>>()
        currentState.addAll(mViewCommands.currentState)
        mViewStates[view] = currentState
    }

    fun destroyView(view: View) {
        mViewStates.remove(view)
    }

    /**
     * Check if view is in restore state or not
     *
     * @param view view for check
     * @return true if view state restore state to incoming view. false otherwise.
     */
    fun isInRestoreState(view: View): Boolean {
        return mInRestoreState.contains(view)
    }
}
