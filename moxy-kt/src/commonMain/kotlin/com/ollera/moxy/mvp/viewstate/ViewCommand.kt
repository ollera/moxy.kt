package com.ollera.moxy.mvp.viewstate

import com.ollera.moxy.mvp.MvpView
import com.ollera.moxy.mvp.viewstate.strategy.StateStrategy
import kotlin.reflect.KClass

abstract class ViewCommand<View : MvpView> protected constructor(val tag: String, val strategyType: KClass<out StateStrategy>) {
    abstract fun apply(view: View)
}
