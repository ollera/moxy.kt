package com.ollera.moxy.mvp

import com.ollera.moxy.mvp.presenter.PresenterField

abstract class PresenterBinder<PresentersContainer> {
    abstract val presenterFields: List<PresenterField<PresentersContainer>>
}
