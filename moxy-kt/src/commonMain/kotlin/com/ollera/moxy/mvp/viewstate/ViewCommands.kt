package com.ollera.moxy.mvp.viewstate

import com.ollera.moxy.mvp.MvpView
import com.ollera.moxy.mvp.reflector.MoxyReflector
import com.ollera.moxy.mvp.viewstate.strategy.StateStrategy

import kotlin.reflect.KClass

class ViewCommands<View : MvpView> {

    val currentState: MutableList<ViewCommand<View>> = mutableListOf()
    private val mStrategies = hashMapOf<KClass<out StateStrategy>, StateStrategy?>()

    val isEmpty: Boolean
        get() = currentState.isEmpty()

    fun beforeApply(viewCommand: ViewCommand<View>) {
        val stateStrategy = getStateStrategy(viewCommand)
        stateStrategy?.beforeApply(currentState, viewCommand)
    }

    fun afterApply(viewCommand: ViewCommand<View>) {
        val stateStrategy = getStateStrategy(viewCommand)
        stateStrategy?.afterApply(currentState, viewCommand)
    }

    private fun getStateStrategy(viewCommand: ViewCommand<View>): StateStrategy? {
        var stateStrategy: StateStrategy? = MoxyReflector.getStrategy(viewCommand.strategyType) as StateStrategy?
        if (stateStrategy == null) {
            try {
                stateStrategy = viewCommand.strategyType.constructors.first().call()
            } catch (e: Exception) {
                throw IllegalArgumentException("Unable to create state strategy: $viewCommand")
            }
            mStrategies[viewCommand.strategyType] = stateStrategy
        }
        return stateStrategy
    }

    fun reapply(view: View, currentState: Set<ViewCommand<View>>) {
        val commands = ArrayList(this.currentState)
        for (command in commands) {
            if (currentState.contains(command)) {
                continue
            }
            command.apply(view)
            afterApply(command)
        }
    }
}
