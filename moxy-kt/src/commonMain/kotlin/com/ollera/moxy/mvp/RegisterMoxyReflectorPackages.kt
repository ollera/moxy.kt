package com.ollera.moxy.mvp

/**
 * Register MoxyReflector packages from other modules
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class RegisterMoxyReflectorPackages(vararg val value: String)
