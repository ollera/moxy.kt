package com.ollera.moxy.mvp.storage

expect class StateStorage() {
    fun getStateStorage(key: String): StateStorage?
    fun containsKey(key: String): Boolean
    fun getString(key: String): String
    fun putStateStorage(key: String, storage: StateStorage)
    fun putAll(storage: StateStorage?)
    fun putString(key: String, value: String)
}