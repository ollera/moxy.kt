package com.ollera.moxy.mvp

object MvpFacade {
    var presenterStore: PresenterStore = PresenterStore()
    var mvpProcessor: MvpProcessor = MvpProcessor()
    var presentersCounter: PresentersCounter = PresentersCounter()
}
