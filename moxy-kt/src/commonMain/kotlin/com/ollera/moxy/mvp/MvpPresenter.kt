package com.ollera.moxy.mvp

import com.ollera.moxy.createWeakSet
import com.ollera.moxy.mvp.presenter.PresenterType
import com.ollera.moxy.mvp.reflector.MoxyReflector
import com.ollera.moxy.mvp.viewstate.MvpViewState
import kotlin.reflect.KClass

abstract class MvpPresenter<View : MvpView> {
    private var mFirstLaunch = true
    internal var tag: String? = null
    internal var presenterType: PresenterType? = null
    private val mViews: MutableSet<View>
    /**
     * @return view state, casted to view interface for simplify
     */
    var viewState: View? = null
        private set
    private var mViewState: MvpViewState<View>? = null
    internal var presenterClass: KClass<out MvpPresenter<View>>? = null

    /**
     * @return views attached to view state, or attached to presenter(if view state not exists)
     */
    val attachedViews: Set<View>
        get() = if (mViewState != null) {
            mViewState!!.views
        } else mViews

    init {
        Binder.bind(this)
        mViews = createWeakSet()
    }

    /**
     *
     * Attach view to view state or to presenter(if view state not exists).
     *
     * If you use [MvpDelegate], you should not call this method directly.
     * It will be called on [MvpDelegate.onAttach], if view does not attached.
     *
     * @param view to attachment
     */
    fun attachView(view: View) {
        if (mViewState != null) {
            mViewState!!.attachView(view)
        } else {
            mViews.add(view)
        }
        if (mFirstLaunch) {
            mFirstLaunch = false

            onFirstViewAttach()
        }
    }

    /**
     *
     * Callback after first presenter init and view binding. If this
     * presenter instance will have to attach some view in future, this method
     * will not be called.
     *
     * There you can to interact with [.mViewState].
     */
    protected open fun onFirstViewAttach() {}

    /**
     *
     * Detach view from view state or from presenter(if view state not exists).
     *
     * If you use [MvpDelegate], you should not call this method directly.
     * It will be called on [MvpDelegate.onDetach].
     *
     * @param view view to detach
     */
    fun detachView(view: View) {
        if (mViewState != null) {
            mViewState!!.detachView(view)
        } else {
            mViews.remove(view)
        }
    }

    fun destroyView(view: View) {
        if (mViewState != null) {
            mViewState!!.destroyView(view)
        }
    }

    /**
     * Check if view is in restore state or not
     *
     * @param view view for check
     * @return true if view state restore state to incoming view. false otherwise.
     */
    fun isInRestoreState(view: View): Boolean {

        return if (mViewState != null) {
            mViewState!!.isInRestoreState(view)
        } else false
    }

    /**
     * Set view state to presenter
     *
     * @param viewState that implements type, setted as View generic param
     */
    @Suppress("UNCHECKED_CAST")
    fun setViewState(viewState: MvpViewState<View>) {
        this.viewState = viewState as View
        mViewState = viewState
    }

    /**
     *
     * Called before reference on this presenter will be cleared and instance of presenter
     * will be never used.
     */
    fun onDestroy() {}

    @Suppress("UNCHECKED_CAST")
    private object Binder {
        internal fun <View : MvpView> bind(presenter: MvpPresenter<View>) {
            val viewState = MoxyReflector.getViewState(presenter::class) as View
            presenter.viewState = viewState
            presenter.mViewState = viewState as MvpViewState<View>
        }
    }
}
