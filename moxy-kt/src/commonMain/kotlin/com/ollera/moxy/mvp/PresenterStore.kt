package com.ollera.moxy.mvp

class PresenterStore {
    private val mPresenters = HashMap<String, MvpPresenter<*>>()

    /**
     * Add presenter to storage
     *
     * @param tag      Tag of presenter. Local presenters contains also delegate's tag as prefix
     * @param instance Instance of MvpPresenter implementation to store
     * @param <T>      Type of presenter
    </T> */
    fun <T : MvpPresenter<*>> add(tag: String, instance: T) {
        mPresenters[tag] = instance
    }

    /**
     * Get presenter on existing params
     *
     * @param tag      Tag of presenter. Local presenters contains also delegate's tag as prefix
     * @return         Presenter if it's exists. Null otherwise (if it's no exists)
     */
    operator fun get(tag: String): MvpPresenter<*>? {
        return mPresenters[tag]
    }

    /**
     * Remove presenter from store.
     *
     * @param tag      Tag of presenter. Local presenters contains also delegate's tag as prefix
     * @return         Presenter which was removed
     */
    fun remove(tag: String): MvpPresenter<*>? {
        return mPresenters.remove(tag)
    }
}