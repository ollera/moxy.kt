package com.ollera.moxy.mvp.presenter

import com.ollera.moxy.mvp.MvpPresenter
import kotlin.reflect.KClass

/**
 *
 * Called when Moxy generate presenter tag for search Presenter in [com.ollera.moxy.mvp.PresenterStore].
 *
 * Requirements:
 *
 *  * presenterClass parameter should be equals with presenter field type
 *  * Presenter Types should be same
 *  * Presenter IDs should be equals
 *
 *
 * Note: if this method stay unused after build, then Moxy never use this method and you should check annotation parameters.
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention
annotation class ProvidePresenterTag(val presenterClass: KClass<out MvpPresenter<*>>, val type: PresenterType = PresenterType.LOCAL, val presenterId: String = "")
