package com.ollera.moxy.mvp

import com.ollera.moxy.mvp.presenter.PresenterType
import com.ollera.moxy.mvp.storage.StateStorage

/**
 * This class represents a delegate which you can use to extend Mvp's support to any class.
 *
 *
 * When using an [MvpDelegate], lifecycle methods which should be proxied to the delegate:
 *
 *  * [.onCreate]
 *  * [.onAttach]: inside onStart() of Activity or Fragment
 *  * [.onSaveInstanceState]
 *  * [.onDetach]: inside onDestroy() for Activity or onDestroyView() for Fragment
 *  * [.onDestroy]
 *
 * Every [Any] can only be linked with one [MvpDelegate] instance,
 * so the instance returned from [.MvpDelegate]} should be kept
 * until the Object is destroyed.
 */
@Suppress("unused", "MemberVisibilityCanBePrivate")
class MvpDelegate<Delegated : MvpView>(private val mDelegated: Delegated) {

    private var mKeyTag = KEY_TAG
    private var mDelegateTag: String = ""
    private var mIsAttached: Boolean = false
    private var mParentDelegate: MvpDelegate<*>? = null
    private var mPresenters: List<MvpPresenter<in Delegated>>? = null
    private var mChildDelegates: MutableList<MvpDelegate<*>> = mutableListOf()
    var childrenSaveState: StateStorage? = null
        private set

    fun setParentDelegate(delegate: MvpDelegate<*>, childId: String) {
        if (childrenSaveState != null) {
            throw IllegalStateException("You should call setParentDelegate() before first onCreate()")
        }
        if (mChildDelegates.size > 0) {
            throw IllegalStateException("You could not set parent delegate when there are already has child presenters")
        }

        mParentDelegate = delegate
        mKeyTag = mParentDelegate!!.mKeyTag + "$" + childId

        delegate.addChildDelegate(this)
    }

    private fun addChildDelegate(delegate: MvpDelegate<*>) {
        mChildDelegates.add(delegate)
    }

    private fun removeChildDelegate(delegate: MvpDelegate<*>) {
        mChildDelegates.remove(delegate)
    }

    /**
     * Free self link from children list (mChildDelegates) in parent delegate
     * property mParentDelegate stay keep link to parent delegate for access to
     * parent bundle for save state in [.onSaveInstanceState]
     */
    fun freeParentDelegate() {
        mParentDelegate?.removeChildDelegate(this)
    }

    fun removeAllChildDelegates() {
        // For avoiding ConcurrentModificationException when removing by removeChildDelegate()
        val childDelegatesClone = ArrayList<MvpDelegate<*>>(mChildDelegates.size)
        childDelegatesClone.addAll(mChildDelegates)

        for (childDelegate in childDelegatesClone) {
            childDelegate.freeParentDelegate()
        }

        mChildDelegates = ArrayList()
    }

    /**
     *
     * Similar like [.onCreate]. But this method try to get saved
     * state from parent presenter before get presenters
     */
    fun onCreate() {
        var storage: StateStorage? = StateStorage()
        if (mParentDelegate != null) {
            storage = mParentDelegate!!.childrenSaveState
        }

        onCreate(storage)
    }

    /**
     *
     * Get(or create if not exists) presenters for delegated object and bind
     * them to this object fields
     *
     * @param stateStorage with saved state
     */
    fun onCreate(stateStorage: StateStorage?) {
        var storage = stateStorage
        if (mParentDelegate == null && storage != null) {
            storage = storage.getStateStorage(MOXY_DELEGATE_TAGS_KEY)
        }

        mIsAttached = false

        val childSaveState = storage ?: StateStorage()
        childrenSaveState = childSaveState

        //get base tag for presenters
        val delegateTag = if (storage == null || !childSaveState.containsKey(mKeyTag)) {
            generateTag()
        } else {
            storage.getString(mKeyTag)
        }
        mDelegateTag = delegateTag

        //bind presenters to view
        mPresenters = MvpFacade.mvpProcessor.getMvpPresenters(mDelegated, delegateTag)

        for (childDelegate in mChildDelegates) {
            childDelegate.onCreate(storage)
        }
    }

    /**
     *
     * Attach delegated object as view to presenter fields of this object.
     * If delegate did not enter at [.onCreate](or
 * [.onCreate]) before this method, then view will not be attached to
     * presenters
     */
    fun onAttach() {
        for (presenter in mPresenters!!) {
            if (mIsAttached && presenter.attachedViews.contains(mDelegated)) {
                continue
            }

            presenter.attachView(mDelegated)
        }

        for (childDelegate in mChildDelegates) {
            childDelegate.onAttach()
        }

        mIsAttached = true
    }

    /**
     *
     * Detach delegated object from their presenters.
     */
    fun onDetach() {
        for (presenter in mPresenters!!) {
            if (!mIsAttached && !presenter.attachedViews.contains(mDelegated)) {
                continue
            }

            presenter.detachView(mDelegated)
        }

        mIsAttached = false

        for (childDelegate in mChildDelegates) {
            childDelegate.onDetach()
        }
    }

    /**
     *
     * View was being destroyed, but logical unit still alive
     */
    fun onDestroyView() {
        for (presenter in mPresenters!!) {
            presenter.destroyView(mDelegated)
        }

        // For avoiding ConcurrentModificationException when removing from mChildDelegates
        val childDelegatesClone = ArrayList<MvpDelegate<*>>(mChildDelegates.size)
        childDelegatesClone.addAll(mChildDelegates)

        for (childDelegate in childDelegatesClone) {
            childDelegate.onDestroyView()
        }

        if (mParentDelegate != null) {
            freeParentDelegate()
        }
    }

    /**
     *
     * Destroy presenters.
     */
    fun onDestroy() {
        val presentersCounter = MvpFacade.presentersCounter
        val presenterStore = MvpFacade.presenterStore

        val allChildPresenters = presentersCounter.getAll(mDelegateTag)
        for (presenter in allChildPresenters) {
            val isRejected = presentersCounter.rejectPresenter(presenter, mDelegateTag)
            if (isRejected && presenter.presenterType !== PresenterType.GLOBAL) {
                presenter.tag?.let { tag ->
                    presenterStore.remove(tag)
                }
                presenter.onDestroy()
            }
        }
    }

    /**
     *
     * Similar like [.onSaveInstanceState]. But this method try to save
     * state to parent presenter Bundle
     */
    fun onSaveInstanceState() {
        var stateStorage = StateStorage()
        if (mParentDelegate != null && mParentDelegate?.childrenSaveState != null) {
            stateStorage = mParentDelegate?.childrenSaveState ?: StateStorage()
        }

        onSaveInstanceState(stateStorage)
    }

    /**
     * Save presenters tag prefix to save state for restore presenters at future after delegate recreate
     *
     * @param stateStorage out state from Android component
     */
    fun onSaveInstanceState(stateStorage: StateStorage) {
        var outState = stateStorage
        if (mParentDelegate == null) {
            val moxyDelegateBundle = StateStorage()
            outState.putStateStorage(MOXY_DELEGATE_TAGS_KEY, moxyDelegateBundle)
            outState = moxyDelegateBundle
        }

        outState.putAll(childrenSaveState)
        outState.putString(mKeyTag, mDelegateTag)

        for (childDelegate in mChildDelegates) {
            childDelegate.onSaveInstanceState(outState)
        }
    }

    /**
     * @return generated tag in format: &lt;parent_delegate_tag&gt; &lt;delegated_class_full_name&gt;$MvpDelegate@&lt;hashCode&gt;
     *
     *
     * example: com.ollera.moxy.mvp.sample.SampleFragment$MvpDelegate@32649b0
     */
    private fun generateTag(): String {
        var tag = mParentDelegate?.mDelegateTag?.plus(" ")?: ""
        tag += mDelegated::class.simpleName + "$" + this::class.simpleName + toString().replace(this::class.qualifiedName
                ?: "", "")
        return tag
    }

    companion object {
        private const val KEY_TAG = "com.ollera.moxy.mvp.MvpDelegate.KEY_TAG"
        const val MOXY_DELEGATE_TAGS_KEY = "MoxyDelegateBundle"
    }
}
