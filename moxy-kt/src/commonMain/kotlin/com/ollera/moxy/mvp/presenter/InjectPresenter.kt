package com.ollera.moxy.mvp.presenter

@Target(AnnotationTarget.FIELD)
@Retention
annotation class InjectPresenter(val tag: String = "", val type: PresenterType = PresenterType.LOCAL, val presenterId: String = "")
