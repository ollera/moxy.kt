package com.ollera.moxy.mvp.reflector

import kotlin.reflect.KClass

object MoxyReflector {
    private var sViewStateProviders: Map<KClass<*>, Any> = HashMap()
    private var sPresenterBinders: Map<KClass<*>, List<Any>> = HashMap()
    private var sStrategies: Map<KClass<*>, Any> = HashMap()

    fun getViewState(presenterClass: KClass<*>): Any? = sViewStateProviders[presenterClass]
    fun getPresenterBinders(delegated: KClass<*>): List<Any>? = sPresenterBinders[delegated]
    fun getStrategy(strategyClass: KClass<*>): Any? = sStrategies.get(strategyClass)
}