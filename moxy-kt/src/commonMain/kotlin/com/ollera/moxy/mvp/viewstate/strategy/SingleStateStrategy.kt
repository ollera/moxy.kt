package com.ollera.moxy.mvp.viewstate.strategy

import com.ollera.moxy.mvp.MvpView
import com.ollera.moxy.mvp.viewstate.ViewCommand

/**
 * This strategy will clear current commands queue and then incoming command will be put in.
 *
 * Caution! Be sure that you fully set view to initial state inside this command.
 */
class SingleStateStrategy : StateStrategy {
    override fun <View : MvpView> beforeApply(currentState: MutableList<ViewCommand<View>>, incomingCommand: ViewCommand<View>) {
        currentState.clear()
        currentState.add(incomingCommand)
    }

    override fun <View : MvpView> afterApply(currentState: MutableList<ViewCommand<View>>, incomingCommand: ViewCommand<View>) {
        // pass
    }
}
