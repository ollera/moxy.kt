package com.ollera.moxy.mvp.viewstate.strategy

import kotlin.reflect.KClass

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE, AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@Retention
annotation class StateStrategyType(val value: KClass<out StateStrategy>, val tag: String = "")
