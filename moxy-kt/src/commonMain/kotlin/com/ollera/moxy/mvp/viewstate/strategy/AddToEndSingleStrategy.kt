package com.ollera.moxy.mvp.viewstate.strategy

import com.ollera.moxy.mvp.MvpView
import com.ollera.moxy.mvp.viewstate.ViewCommand

/**
 * Command will be added to end of commands queue. If commands queue contains same type command, then existing command will be removed.
 */
class AddToEndSingleStrategy : StateStrategy {
    override fun <View : MvpView> beforeApply(currentState: MutableList<ViewCommand<View>>, incomingCommand: ViewCommand<View>) {
        val iterator = currentState.iterator()

        while (iterator.hasNext()) {
            val entry = iterator.next()

            if (entry::class == incomingCommand::class) {
                iterator.remove()
                break
            }
        }

        currentState.add(incomingCommand)
    }

    override fun <View : MvpView> afterApply(currentState: MutableList<ViewCommand<View>>, incomingCommand: ViewCommand<View>) {
        // pass
    }
}
