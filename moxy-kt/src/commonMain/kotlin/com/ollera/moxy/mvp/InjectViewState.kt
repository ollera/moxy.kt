package com.ollera.moxy.mvp

import com.ollera.moxy.mvp.viewstate.MvpViewState
import kotlin.reflect.KClass

/**
 * Inject view state to [MvpPresenter.mViews] and
 * [MvpPresenter.mViewState] presenter fields. Presenter, annotated with
 * this, should be strongly typed on view interface(not write some like extends
 * MvpPresenter&lt;V extends SuperView&gt;). Otherwise code generation make
 * code, that broke your app.
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class InjectViewState(val value: KClass<out MvpViewState<*>> = DefaultViewState::class, val view: KClass<out MvpView> = DefaultView::class)
