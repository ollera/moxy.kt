package com.ollera.moxy.mvp.presenter

import com.ollera.moxy.mvp.MvpPresenter
import kotlin.reflect.KClass

abstract class PresenterField<PresentersContainer> protected constructor(protected val tag: String, val presenterType: PresenterType, val presenterId: String, val presenterClass: KClass<out MvpPresenter<*>>) {
    open fun getTag(delegated: PresentersContainer) = tag
    abstract fun bind(container: PresentersContainer, presenter: MvpPresenter<*>)
    abstract fun providePresenter(delegated: PresentersContainer): MvpPresenter<*>?
}
