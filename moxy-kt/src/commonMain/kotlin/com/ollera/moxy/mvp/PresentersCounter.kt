package com.ollera.moxy.mvp

class PresentersCounter {

    private val mConnections = mutableMapOf<MvpPresenter<*>, MutableSet<String>>()
    private val mTags = mutableMapOf<String, MutableSet<MvpPresenter<*>>>()

    /**
     * Save delegate tag when it inject presenter to delegate's object
     *
     * @param presenter     Injected presenter
     * @param delegateTag   Delegate tag
     */
    fun injectPresenter(presenter: MvpPresenter<*>, delegateTag: String) {
        var delegateTags: MutableSet<String>? = mConnections[presenter]
        if (delegateTags == null) {
            delegateTags = HashSet()
            mConnections[presenter] = delegateTags
        }

        delegateTags.add(delegateTag)

        var presenters: MutableSet<MvpPresenter<*>>? = mTags[delegateTag]
        if (presenters == null) {
            presenters = HashSet()
            mTags[delegateTag] = presenters
        }
        presenters.add(presenter)
    }

    /**
     * Remove tag when delegate's object was fully destroyed
     *
     * @param presenter     Rejected presenter
     * @param delegateTag   Delegate tag
     * @return              True if there are no links to this presenter and presenter be able to destroy. False otherwise
     */
    fun rejectPresenter(presenter: MvpPresenter<*>, delegateTag: String): Boolean {
        val presenters = mTags[delegateTag]
        presenters?.remove(presenter)
        if (presenters == null || presenters.isEmpty()) {
            mTags.remove(delegateTag)
        }

        val delegateTags = mConnections[presenter]
        if (delegateTags == null) {
            mConnections.remove(presenter)
            return true
        }

        val tagsIterator = delegateTags.iterator()
        while (tagsIterator.hasNext()) {
            val tag = tagsIterator.next()

            if (tag.startsWith(delegateTag)) {
                tagsIterator.remove()
            }
        }

        val noTags = delegateTags.isEmpty()

        if (noTags) {
            mConnections.remove(presenter)
        }

        return noTags
    }

    fun getAll(delegateTag: String): Set<MvpPresenter<*>> {
        val presenters = HashSet<MvpPresenter<*>>()
        for ((key, value) in mTags) {
            if (key.startsWith(delegateTag)) {
                presenters.addAll(value)
            }
        }

        return presenters
    }

    fun isInjected(presenter: MvpPresenter<*>): Boolean {
        val mDelegateTags = mConnections[presenter]

        return mDelegateTags != null && !mDelegateTags.isEmpty()
    }
}
