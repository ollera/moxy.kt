package com.ollera.moxy.mvp.presenter

import com.ollera.moxy.mvp.MvpPresenter

/**
 * Available presenter types. Manually lifetime control are available over [com.ollera.moxy.mvp.PresenterStore], [com.ollera.moxy.mvp.PresentersCounter] and [MvpPresenter.onDestroy]
 */
enum class PresenterType {
    /**
     * Local presenter are not available out of injectable object
     */
    LOCAL,
    /**
     * Weak presenters are available everywhere. Weak presenter will be destroyed when finished all views. Inject will create new presenter instance.
     */
    WEAK,
    /**
     * Global presenter will be destroyed only when process will be killed([MvpPresenter.onDestroy] won't be called)
     */
    GLOBAL
}
