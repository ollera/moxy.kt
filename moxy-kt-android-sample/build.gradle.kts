plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    kotlin("kapt")
}

val kotlinVersion: String = properties["kotlinVersion"] as String

android {
    compileSdkVersion(29)
    buildToolsVersion("29.0.2")


    defaultConfig {
        applicationId = "com.ollera.moxy.android.sample"
        minSdkVersion(15)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"
    }
}

dependencies {
    implementation("androidx.appcompat:appcompat:1.0.2")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation(project(":moxy-kt", "jvmDefault"))
    kapt(project(":moxy-kt-processor", "jvmDefault"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion")
}
