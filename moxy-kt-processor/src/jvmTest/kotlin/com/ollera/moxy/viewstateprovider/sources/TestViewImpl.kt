package com.ollera.moxy.viewstateprovider.sources

import com.ollera.moxy.mvp.MvpDelegate
import com.ollera.moxy.mvp.presenter.InjectPresenter

class TestViewImpl() : TestView {

    @InjectPresenter
    lateinit var presenter: TestPresenter

    private val mMvpDelegate: MvpDelegate<TestView> by lazy { MvpDelegate<TestView>(this) }

    init {
        println("Init")
        onCreate()
        onResume()
    }

    fun onCreate() {
        println("onCreate")
        mMvpDelegate.onCreate()
    }

    fun onResume() {
        println("onResume")
        mMvpDelegate.onAttach()
    }
}
