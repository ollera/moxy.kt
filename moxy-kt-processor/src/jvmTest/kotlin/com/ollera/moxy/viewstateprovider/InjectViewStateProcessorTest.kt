package com.ollera.moxy.viewstateprovider

import com.ollera.moxy.processor.MvpProcessor
import com.tschuchort.compiletesting.KotlinCompilation
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.io.File
import java.net.URLClassLoader

@Suppress("UnstableApiUsage")
class InjectViewStateProcessorTest {
    @Test
    fun testProcessor() {
        val kaptTestDir = File("./build/testKapt")
        val source = File("./src/jvmTest/kotlin/com/ollera/moxy/viewstateprovider/sources")
        val kaptTestSourcedDir = File(kaptTestDir, "sources")
        source.copyRecursively(kaptTestSourcedDir, true)
        val result = KotlinCompilation().apply {
            workingDir = kaptTestDir
            jdkHome = File(System.getProperty("java.home")).parentFile
            annotationProcessors = listOf(MvpProcessor())
            inheritClassPath = true
            messageOutputStream = System.out
        }.compile()
        println(result.outputDirectory)
        Assertions.assertEquals(result.exitCode, KotlinCompilation.ExitCode.OK)
        result.runCompiled("com.ollera.moxy.viewstateprovider.sources.TestViewImpl")
    }

    fun KotlinCompilation.Result.runCompiled(className: String) {
        val scriptClassLoader = URLClassLoader(arrayOf(outputDirectory.toURI().toURL()), Thread.currentThread().contextClassLoader)
        val scriptClass = try {
            scriptClassLoader.loadClass(className)
        } catch (e: ClassNotFoundException) {
            throw IllegalArgumentException("Could not load script class for given filename $className", e)
        }
        scriptClass.getConstructor().newInstance()
    }
}