package com.ollera.moxy.viewstateprovider.sources

import com.ollera.moxy.mvp.InjectViewState
import com.ollera.moxy.mvp.MvpPresenter

@InjectViewState
class TestPresenter: MvpPresenter<TestView>() {

}