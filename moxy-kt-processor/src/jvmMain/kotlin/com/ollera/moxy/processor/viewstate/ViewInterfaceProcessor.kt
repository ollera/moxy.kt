package com.ollera.moxy.processor.viewstate

import com.ollera.moxy.mvp.viewstate.strategy.AddToEndStrategy
import com.ollera.moxy.mvp.viewstate.strategy.StateStrategyType
import com.ollera.moxy.processor.ElementProcessor
import com.ollera.moxy.processor.MvpProcessor
import com.ollera.moxy.processor.Util
import com.ollera.moxy.processor.Util.getAnnotation
import com.ollera.moxy.processor.Util.getAnnotationValueAsString
import com.ollera.moxy.processor.Util.getAnnotationValueAsTypeMirror
import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet

import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.Modifier
import javax.lang.model.element.TypeElement
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.TypeKind
import javax.tools.Diagnostic

class ViewInterfaceProcessor : ElementProcessor<TypeElement, ViewInterfaceInfo>() {

    private var viewInterfaceElement: TypeElement? = null
    private var viewInterfaceName: String? = null
    private val usedStrategies = HashSet<TypeElement>()

    fun getUsedStrategies(): List<TypeElement> {
        return ArrayList(usedStrategies)
    }

    override fun process(element: TypeElement): ViewInterfaceInfo {
        this.viewInterfaceElement = element
        viewInterfaceName = element.simpleName.toString()

        val methods = ArrayList<ViewMethod>()

        val interfaceStateStrategyType = getInterfaceStateStrategyType(element)

        // Get methods for input class
        getMethods(element, interfaceStateStrategyType, ArrayList(), methods)

        // Add methods from super interfaces
        methods.addAll(iterateInterfaces(0, element, interfaceStateStrategyType, methods, ArrayList()))

        // Allow methods be with same names
        val methodsCounter = HashMap<String, Int>()
        for (method in methods) {
            var counter: Int? = methodsCounter[method.name]

            if (counter != null && counter > 0) {
                method.uniqueSuffix = counter.toString()
            } else {
                counter = 0
            }

            counter++
            methodsCounter[method.name] = counter
        }

        return ViewInterfaceInfo(element, methods)
    }


    private fun getMethods(typeElement: TypeElement,
                           defaultStrategy: TypeElement?,
                           rootMethods: List<ViewMethod>,
                           superinterfacesMethods: MutableList<ViewMethod>) {
        for (element in typeElement.enclosedElements) {
            // ignore all but non-static methods
            if (element.kind != ElementKind.METHOD || element.modifiers.contains(Modifier.STATIC)) {
                continue
            }

            val methodElement = element as ExecutableElement

            if (methodElement.returnType.kind != TypeKind.VOID) {
                val message = String.format("You are trying generate ViewState for %s. " +
                        "But %s contains non-void method \"%s\" that return type is %s. " +
                        "See more here: https://github.com/Arello-Mobile/Moxy/issues/2",
                        typeElement.simpleName,
                        typeElement.simpleName,
                        methodElement.simpleName,
                        methodElement.returnType
                )
                MvpProcessor.messager.printMessage(Diagnostic.Kind.ERROR, message)
            }

            val annotation = getAnnotation(methodElement, STATE_STRATEGY_TYPE_ANNOTATION)

            // get strategy from annotation
            val strategyClassFromAnnotation = getAnnotationValueAsTypeMirror(annotation, "value")

            val strategyClass: TypeElement
            strategyClass = if (strategyClassFromAnnotation != null) {
                (strategyClassFromAnnotation as DeclaredType).asElement() as TypeElement
            } else {
                defaultStrategy ?: DEFAULT_STATE_STRATEGY
            }

            // get tag from annotation
            val tagFromAnnotation = getAnnotationValueAsString(annotation, "tag")

            val methodTag = tagFromAnnotation ?: methodElement.simpleName.toString()

            // add strategy to list
            usedStrategies.add(strategyClass)

            val method = ViewMethod(
                    viewInterfaceElement!!.asType() as DeclaredType, methodElement, strategyClass, methodTag
            )

            if (rootMethods.contains(method)) {
                continue
            }

            if (superinterfacesMethods.contains(method)) {
                checkStrategyAndTagEquals(method, superinterfacesMethods[superinterfacesMethods.indexOf(method)])
                continue
            }

            superinterfacesMethods.add(method)
        }
    }

    private fun checkStrategyAndTagEquals(method: ViewMethod, existingMethod: ViewMethod) {
        val differentParts = mutableListOf<String>()
        if (existingMethod.strategy != method.strategy) {
            differentParts.add("strategies")
        }
        if (existingMethod.tag != method.tag) {
            differentParts.add("tags")
        }

        if (differentParts.isNotEmpty()) {
            val arguments = method.parameterSpecs.joinToString(", ") { it.toString() }
            val parts = differentParts.joinToString(" and ")
            throw IllegalStateException("Both " + existingMethod.enclosedClassName +
                    " and " + method.enclosedClassName +
                    " has method " + method.name + "(" + arguments + ")" +
                    " with different " + parts + "." +
                    " Override this method in " + viewInterfaceName + " or make " + parts + " equals")
        }
    }

    private fun iterateInterfaces(level: Int,
                                  parentElement: TypeElement,
                                  parentDefaultStrategy: TypeElement?,
                                  rootMethods: List<ViewMethod>,
                                  superinterfacesMethods: MutableList<ViewMethod>): List<ViewMethod> {
        for (typeMirror in parentElement.interfaces) {
            val anInterface = (typeMirror as DeclaredType).asElement() as TypeElement

            val typeArguments = typeMirror.typeArguments
            val typeParameters = anInterface.typeParameters

            if (typeArguments.size > typeParameters.size) {
                throw IllegalArgumentException("Code generation for interface " + anInterface.simpleName + " failed. Simplify your generics.")
            }
            val defaultStrategy = parentDefaultStrategy ?: getInterfaceStateStrategyType(anInterface)
            getMethods(anInterface, defaultStrategy, rootMethods, superinterfacesMethods)
            iterateInterfaces(level + 1, anInterface, defaultStrategy, rootMethods, superinterfacesMethods)
        }

        return superinterfacesMethods
    }

    private fun getInterfaceStateStrategyType(typeElement: TypeElement): TypeElement? {
        val annotation = getAnnotation(typeElement, STATE_STRATEGY_TYPE_ANNOTATION)
        val value = getAnnotationValueAsTypeMirror(annotation, "value")
        return if (value != null && value.kind == TypeKind.DECLARED) {
            (value as DeclaredType).asElement() as TypeElement
        } else {
            null
        }
    }

    companion object {
        private val STATE_STRATEGY_TYPE_ANNOTATION = StateStrategyType::class.java.name
        private val DEFAULT_STATE_STRATEGY = MvpProcessor.elementUtils.getTypeElement(AddToEndStrategy::class.java.canonicalName)
    }
}
