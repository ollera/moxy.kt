package com.ollera.moxy.processor.viewstate

import com.ollera.moxy.processor.MvpProcessor
import com.squareup.kotlinpoet.*

import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.ExecutableType

class ViewMethod(targetInterfaceElement: DeclaredType,
                          val element: ExecutableElement,
                          val strategy: TypeElement,
                          val tag: String) {
    val name: String = element.simpleName.toString()
    val parameterSpecs: MutableList<ParameterSpec> = mutableListOf()
    val exceptions: List<TypeName>
    val typeVariables: List<TypeVariableName>
    val argumentsString: String

    var uniqueSuffix: String? = null

    val commandClassName: String
        get() = name.substring(0, 1).toUpperCase() + name.substring(1) + uniqueSuffix + "Command"

    val enclosedClassName: String
        get() {
            val typeElement = element.enclosingElement as TypeElement
            return typeElement.qualifiedName.toString()
        }

    init {

        val typeUtils = MvpProcessor.typeUtils
        val executableType = typeUtils.asMemberOf(targetInterfaceElement, element) as ExecutableType
        val parameters = element.parameters
        val resolvedParameterTypes = executableType.parameterTypes

        for (i in parameters.indices) {
            val element = parameters[i]
            val type = resolvedParameterTypes[i].asTypeName()
            val name = element.simpleName.toString()

            parameterSpecs.add(ParameterSpec.builder(name, type)
                    .jvmModifiers(element.modifiers)
                    .build()
            )
        }

        this.exceptions = element.thrownTypes.map { it.asTypeName() }

        this.typeVariables = element.typeParameters.map { it.asTypeVariableName() }
        this.argumentsString = this.parameterSpecs.joinToString(", ") { parameterSpec -> parameterSpec.name }
        this.uniqueSuffix = ""
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false

        val that = other as ViewMethod?

        return name == that!!.name && parameterSpecs == that.parameterSpecs
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + parameterSpecs.hashCode()
        return result
    }
}
