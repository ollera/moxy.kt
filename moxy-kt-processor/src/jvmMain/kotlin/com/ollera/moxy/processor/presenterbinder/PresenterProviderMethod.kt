package com.ollera.moxy.processor.presenterbinder

import com.ollera.moxy.mvp.presenter.PresenterType
import javax.lang.model.type.DeclaredType

internal class PresenterProviderMethod(val clazz: DeclaredType, val name: String, type: String?, val tag: String, val presenterId: String) {
    val presenterType: PresenterType = if (type == null) {
        PresenterType.LOCAL
    } else {
        PresenterType.valueOf(type)
    }
}