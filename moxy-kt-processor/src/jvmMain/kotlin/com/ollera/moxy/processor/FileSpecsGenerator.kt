package com.ollera.moxy.processor

import com.squareup.kotlinpoet.FileSpec

abstract class FileSpecsGenerator<T> {
    abstract fun generate(input: T): List<FileSpec>
}