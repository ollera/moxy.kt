package com.ollera.moxy.processor

import com.ollera.moxy.mvp.MvpPresenter
import com.ollera.moxy.mvp.MvpView
import com.ollera.moxy.mvp.presenter.InjectPresenter
import com.ollera.moxy.processor.Util.fillGenerics
import com.ollera.moxy.processor.Util.getFullClassName
import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet
import javax.lang.model.element.*
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.TypeKind
import javax.lang.model.type.TypeMirror
import javax.lang.model.type.TypeVariable
import javax.tools.Diagnostic

class PresenterInjectorRules(validKind: ElementKind, vararg validModifiers: Modifier) : AnnotationRule(validKind, *validModifiers) {

    override fun checkAnnotation(annotatedField: Element) {
        checkEnvironment(annotatedField)
        if (annotatedField.kind != mValidKind) {
            mErrorBuilder.append("Field " + annotatedField + " of " + annotatedField.enclosingElement.simpleName + " should be " + mValidKind.name + ", or not mark it as @" + InjectPresenter::class.java.simpleName).append("\n")
        }
        for (modifier in annotatedField.modifiers) {
            if (!mValidModifiers.contains(modifier)) {
                mErrorBuilder.append("Field " + annotatedField + " of " + annotatedField.enclosingElement.simpleName + " can't be a " + modifier).append(". Use ").append(validModifiersToString()).append("\n")
            }
        }
        var enclosingElement = annotatedField.enclosingElement
        while (enclosingElement.kind == ElementKind.CLASS) {
            if (!enclosingElement.modifiers.contains(Modifier.PUBLIC)) {
                mErrorBuilder.append(enclosingElement.simpleName.toString() + " should be PUBLIC ")
                break
            }
            enclosingElement = enclosingElement.enclosingElement
        }
    }

    private fun checkEnvironment(annotatedField: Element) {
        if (annotatedField.asType() !is DeclaredType) {
            return
        }
        val typeElement = (annotatedField.asType() as DeclaredType).asElement() as TypeElement
        val viewClassFromGeneric = getViewClassFromGeneric(typeElement, annotatedField.asType() as DeclaredType)
        val viewsType = getViewsType((annotatedField.enclosingElement.asType() as DeclaredType).asElement() as TypeElement)
        var result = false
        for (typeMirror in viewsType) {
            if (getFullClassName(typeMirror) == viewClassFromGeneric || fillGenerics(emptyMap(), typeMirror) == viewClassFromGeneric) {
                result = true
                break
            }
        }
        if (!result) {
            MvpProcessor.messager.printMessage(Diagnostic.Kind.ERROR, "You can not use @InjectPresenter in classes that are not View, which is typified target Presenter", annotatedField)
        }
    }

    private fun getViewClassFromGeneric(typeElement: TypeElement, declaredType: DeclaredType): String {
        var superclass: TypeMirror = declaredType
        var mTypedMap = emptyMap<TypeParameterElement, TypeMirror>()
        if (typeElement.typeParameters.isNotEmpty()) {
            mTypedMap = getChildInstanceOfClassFromGeneric(typeElement, MvpView::class.java)
        }
        var parentTypes = emptyMap<String, String>()
        val totalTypeArguments = ArrayList((superclass as DeclaredType).typeArguments)
        while (superclass.kind != TypeKind.NONE) {
            val superclassElement = (superclass as DeclaredType).asElement() as TypeElement
            val typeArguments = superclass.typeArguments
            totalTypeArguments.retainAll(typeArguments)
            val typeParameters = superclassElement.typeParameters
            val types = HashMap<String, String>()
            for (i in typeArguments.indices) {
                types[typeParameters[i].toString()] = fillGenerics(parentTypes, typeArguments[i])
            }
            if (superclassElement.toString() == MvpPresenter::class.java.canonicalName) {
                if (typeArguments.isNotEmpty()) {
                    val typeMirror = typeArguments[0]
                    if (typeMirror is TypeVariable) {
                        val key = typeMirror.asElement()

                        for ((key1, value) in mTypedMap) {
                            if (key1.toString() == key.toString()) {
                                return getFullClassName(value)
                            }
                        }
                    }
                }
                return if (typeArguments.isEmpty() && typeParameters.isEmpty()) {
                    superclass.asElement().simpleName.toString()
                } else fillGenerics(parentTypes, typeArguments)
            }
            parentTypes = types
            superclass = superclassElement.superclass
        }
        return ""
    }

    private fun getChildInstanceOfClassFromGeneric(typeElement: TypeElement, aClass: Class<*>): Map<TypeParameterElement, TypeMirror> {
        val result = HashMap<TypeParameterElement, TypeMirror>()
        for (element in typeElement.typeParameters) {
            val bounds = element.bounds
            for (bound in bounds) {
                if (bound is DeclaredType && bound.asElement() is TypeElement) {
                    val viewsType = getViewsType(bound.asElement() as TypeElement)
                    var isViewType = false
                    for (viewType in viewsType) {
                        if ((viewType as DeclaredType).asElement().toString() == aClass.canonicalName) {
                            isViewType = true
                        }
                    }
                    if (isViewType) {
                        result[element] = bound
                        break
                    }
                }
            }
        }
        return result
    }

    private fun getViewsType(typeElement: TypeElement): Collection<TypeMirror> {
        var superclass = typeElement.asType()
        val result = ArrayList<TypeMirror>()
        while (superclass.kind != TypeKind.NONE) {
            val superclassElement = (superclass as DeclaredType).asElement() as TypeElement
            val interfaces = HashSet(superclassElement.interfaces)
            for (typeMirror in interfaces) {
                if (typeMirror is DeclaredType) {
                    result.addAll(getViewsType(typeMirror.asElement() as TypeElement))
                }
            }
            result.addAll(interfaces)
            result.add(superclass)
            superclass = superclassElement.superclass
        }
        return result
    }
}