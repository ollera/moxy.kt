package com.ollera.moxy.processor.viewstate


import com.ollera.moxy.mvp.MvpProcessor.Companion.VIEW_STATE_SUFFIX
import com.ollera.moxy.mvp.viewstate.MvpViewState
import com.ollera.moxy.mvp.viewstate.ViewCommand
import com.ollera.moxy.processor.FileSpecsGenerator
import com.ollera.moxy.processor.MvpProcessor
import com.ollera.moxy.processor.Util.decapitalizeString
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import java.util.*
import javax.lang.model.type.DeclaredType

class ViewStateClassGenerator : FileSpecsGenerator<ViewInterfaceInfo>() {

    override fun generate(input: ViewInterfaceInfo): List<FileSpec> {
        val viewName = input.name
        val nameWithTypeVariables = input.nameWithTypeVariables
        val viewInterfaceType = input.element.asType() as DeclaredType

        val classBuilder = TypeSpec.classBuilder(viewName.simpleName + VIEW_STATE_SUFFIX)
                .addModifiers(KModifier.PUBLIC)
                .superclass(MvpViewState::class.asClassName().parameterizedBy(nameWithTypeVariables))
                .addSuperinterface(nameWithTypeVariables)
                .addTypeVariables(input.typeVariables)

        for (method in input.methods) {
            val commandClass = generateCommandClass(method, nameWithTypeVariables)
            classBuilder.addType(commandClass)
            classBuilder.addFunction(generateMethod(viewInterfaceType, method, nameWithTypeVariables, commandClass))
        }

        val javaFile = FileSpec.builder(viewName.packageName, viewName.simpleName)
                .addType(classBuilder.build())
                .indent("\t")
                .build()
        return listOf(javaFile)
    }

    private fun generateCommandClass(method: ViewMethod, viewTypeName: TypeName): TypeSpec {
        val applyMethod = FunSpec.builder("apply")
                .addAnnotation(Override::class.java)
                .addModifiers(KModifier.PUBLIC)
                .addParameter("mvpView", viewTypeName)
                .addStatement("mvpView.\$L(\$L)", method.name, method.argumentsString)
                .build()

        val classBuilder = TypeSpec.classBuilder(method.commandClassName)
                .addModifiers(KModifier.PUBLIC)
                .addTypeVariables(method.typeVariables)
                .superclass(ViewCommand::class.asClassName().parameterizedBy(viewTypeName))
                .addFunction(generateCommandConstructor(method))
                .addFunction(applyMethod)

        for (parameter in method.parameterSpecs) {
            classBuilder.addProperty(parameter.name, parameter.type, KModifier.PUBLIC, KModifier.FINAL)
        }

        return classBuilder.build()
    }

    private fun generateMethod(enclosingType: DeclaredType, method: ViewMethod, viewTypeName: TypeName, commandClass: TypeSpec): FunSpec {
        // TODO: String commandFieldName = "$cmd";
        var commandFieldName = decapitalizeString(method.commandClassName)

        // Add salt if contains argument with same name
        val random = Random()
        while (method.argumentsString.contains(commandFieldName)) {
            commandFieldName += random.nextInt(10)
        }

        return FunSpec.overriding(method.element, enclosingType, MvpProcessor.typeUtils)
                .addStatement("var $2L:\$1N = $1N($3L)", commandClass, commandFieldName, method.argumentsString)
                .addStatement("mViewCommands.beforeApply(\$L)", commandFieldName)
                .addCode("\n")
                .beginControlFlow("if (mViews == null || mViews.isEmpty())")
                .addStatement("return")
                .endControlFlow()
                .addCode("\n")
                .beginControlFlow("for (\$T view : mViews)", viewTypeName)
                .addStatement("view.\$L(\$L)", method.name, method.argumentsString)
                .endControlFlow()
                .addCode("\n")
                .addStatement("mViewCommands.afterApply(\$L)", commandFieldName)
                .build()
    }

    private fun generateCommandConstructor(method: ViewMethod): FunSpec {
        val parameters = method.parameterSpecs

        val builder = FunSpec.constructorBuilder()
                .addParameters(parameters)
                .addStatement("super(\$S, \$T.class)", method.tag, method.strategy)

        if (parameters.size > 0) {
            builder.addCode("\n")
        }

        for (parameter in parameters) {
            builder.addStatement("this.$1N = $1N", parameter)
        }

        return builder.build()
    }

}
