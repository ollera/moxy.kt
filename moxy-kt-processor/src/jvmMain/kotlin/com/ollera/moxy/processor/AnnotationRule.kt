package com.ollera.moxy.processor

import java.util.*
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.Modifier

abstract class AnnotationRule(protected val mValidKind: ElementKind, vararg validModifiers: Modifier) {
    protected val mValidModifiers: Set<Modifier> = HashSet(listOf(*validModifiers))
    protected var mErrorBuilder = StringBuilder()

    val errorStack: String
        get() = mErrorBuilder.toString()

    init {
        if (validModifiers.isEmpty()) {
            throw RuntimeException("Valid modifiers cant be empty or null.")
        }
    }

    abstract fun checkAnnotation(annotatedField: Element)

    protected fun validModifiersToString(): String {
        return if (mValidModifiers.size > 1) {
            val result = StringBuilder("one of [")
            var addSeparator = false
            for (validModifier in mValidModifiers) {
                if (addSeparator) {
                    result.append(", ")
                }
                addSeparator = true
                result.append(validModifier.toString())
            }
            result.append("]")
            result.toString()
        } else {
            mValidModifiers.iterator().next().toString() + "."
        }
    }
}
