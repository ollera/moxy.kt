package com.ollera.moxy.processor.presenterbinder

import com.ollera.moxy.mvp.presenter.InjectPresenter
import com.ollera.moxy.mvp.presenter.PresenterType
import com.ollera.moxy.mvp.presenter.ProvidePresenter
import com.ollera.moxy.mvp.presenter.ProvidePresenterTag
import com.ollera.moxy.processor.ElementProcessor
import com.ollera.moxy.processor.Util.getAnnotation
import com.ollera.moxy.processor.Util.getAnnotationValueAsString
import com.ollera.moxy.processor.Util.getAnnotationValueAsTypeMirror
import java.lang.IllegalStateException
import java.util.ArrayList
import javax.lang.model.element.ElementKind
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.element.VariableElement
import javax.lang.model.type.DeclaredType

class InjectPresenterProcessor : ElementProcessor<VariableElement, TargetClassInfo>() {

    val presentersContainers = mutableListOf<TypeElement>()

    override fun process(element: VariableElement): TargetClassInfo? {
        val enclosingElement = element.enclosingElement
        if (enclosingElement !is TypeElement) {
            throw RuntimeException("Only class fields could be annotated as @InjectPresenter: " +
                    element + " at " + enclosingElement)
        }
        if (presentersContainers.contains(enclosingElement)) {
            return null
        }
        presentersContainers.add(enclosingElement)
        val fields = collectFields(enclosingElement)
        bindProvidersToFields(fields, collectPresenterProviders(enclosingElement))
        bindTagProvidersToFields(fields, collectTagProviders(enclosingElement))
        return TargetClassInfo(enclosingElement, fields)
    }

    companion object {
        private val PRESENTER_FIELD_ANNOTATION = InjectPresenter::class.java.name
        private val PROVIDE_PRESENTER_ANNOTATION = ProvidePresenter::class.java.name
        private val PROVIDE_PRESENTER_TAG_ANNOTATION = ProvidePresenterTag::class.java.name
        private fun collectFields(presentersContainer: TypeElement): List<TargetPresenterField> {
            val fields = ArrayList<TargetPresenterField>()
            for (element in presentersContainer.enclosedElements) {
                if (element.kind != ElementKind.FIELD) {
                    continue
                }
                val annotation = getAnnotation(element, PRESENTER_FIELD_ANNOTATION) ?: continue
                val clazz = (element.asType() as DeclaredType).asElement().asType()
                val name = element.toString()
                val type = getAnnotationValueAsString(annotation, "type") ?: PresenterType.LOCAL.name
                val tag = getAnnotationValueAsString(annotation, "tag") ?: ""
                val presenterId = getAnnotationValueAsString(annotation, "presenterId") ?: ""
                val field = TargetPresenterField(clazz, name, type, tag, presenterId)
                fields.add(field)
            }
            return fields
        }

        private fun collectPresenterProviders(presentersContainer: TypeElement): List<PresenterProviderMethod> {
            val providers = ArrayList<PresenterProviderMethod>()
            for (element in presentersContainer.enclosedElements) {
                if (element.kind != ElementKind.METHOD) {
                    continue
                }
                val providerMethod = element as ExecutableElement
                val annotation = getAnnotation(element, PROVIDE_PRESENTER_ANNOTATION) ?: continue
                val name = providerMethod.simpleName.toString()
                val kind = providerMethod.returnType as DeclaredType
                val type = getAnnotationValueAsString(annotation, "type")
                val tag = getAnnotationValueAsString(annotation, "tag") ?: throw IllegalStateException()
                val presenterId = getAnnotationValueAsString(annotation, "presenterId") ?: throw IllegalStateException()
                providers.add(PresenterProviderMethod(kind, name, type, tag, presenterId))
            }
            return providers
        }

        private fun collectTagProviders(presentersContainer: TypeElement): List<TagProviderMethod> {
            val providers = ArrayList<TagProviderMethod>()
            for (element in presentersContainer.enclosedElements) {
                if (element.kind != ElementKind.METHOD) {
                    continue
                }
                val providerMethod = element as ExecutableElement
                val annotation = getAnnotation(element, PROVIDE_PRESENTER_TAG_ANNOTATION) ?: continue
                val name = providerMethod.simpleName.toString()
                val presenterClass = getAnnotationValueAsTypeMirror(annotation, "presenterClass")
                val type = getAnnotationValueAsString(annotation, "type") ?: PresenterType.LOCAL.name
                val presenterId = getAnnotationValueAsString(annotation, "presenterId") ?: ""
                providers.add(TagProviderMethod(presenterClass, name, type, presenterId))
            }
            return providers
        }

        private fun bindProvidersToFields(fields: List<TargetPresenterField>, presenterProviders: List<PresenterProviderMethod>) {
            if (fields.isEmpty() || presenterProviders.isEmpty()) return
            for (presenterProvider in presenterProviders) {
                val providerTypeMirror = presenterProvider.clazz.asElement().asType()
                for (field in fields) {
                    if (field.clazz == providerTypeMirror) {
                        if (field.presenterType != presenterProvider.presenterType
                                || field.tag.isEmpty() && presenterProvider.tag.isNotEmpty()
                                || field.tag.isNotEmpty() && field.tag != presenterProvider.tag
                                || field.presenterId.isEmpty() && presenterProvider.presenterId.isNotEmpty()
                                || field.presenterId.isNotEmpty() && field.presenterId != presenterProvider.presenterId) {
                            continue
                        }
                        field.presenterProviderMethodName = presenterProvider.name
                    }
                }
            }
        }

        private fun bindTagProvidersToFields(fields: List<TargetPresenterField>, tagProviders: List<TagProviderMethod>) {
            if (fields.isEmpty() || tagProviders.isEmpty()) return
            for (tagProvider in tagProviders) {
                for (field in fields) {
                    if (field.clazz == tagProvider.presenterClass) {
                        if (field.presenterType !== tagProvider.type
                                || field.presenterId.isEmpty() && tagProvider.presenterId.isNotEmpty()
                                || field.presenterId.isNotEmpty() && field.presenterId != tagProvider.presenterId) {
                            continue
                        }
                        field.presenterTagProviderMethodName = tagProvider.methodName
                    }
                }
            }
        }
    }
}
