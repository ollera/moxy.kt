package com.ollera.moxy.processor.presenterbinder

import com.ollera.moxy.mvp.MvpProcessor
import com.ollera.moxy.mvp.presenter.PresenterType
import com.squareup.kotlinpoet.ParameterizedTypeName
import com.squareup.kotlinpoet.TypeName
import com.squareup.kotlinpoet.asTypeName
import javax.lang.model.type.TypeMirror

class TargetPresenterField(val clazz: TypeMirror,
                           val name: String,
                           presenterType: String,
                           val tag: String,
                           val presenterId: String) {
    private val isParametrized: Boolean = clazz.asTypeName() is ParameterizedTypeName
    val typeName: TypeName = if (isParametrized) (clazz.asTypeName() as ParameterizedTypeName).rawType else clazz.asTypeName()
    val presenterType: PresenterType = PresenterType.valueOf(presenterType)
    var presenterProviderMethodName: String? = null
    var presenterTagProviderMethodName: String? = null
    val generatedClassName: String
        get() = name + MvpProcessor.PRESENTER_BINDER_INNER_SUFFIX
}
