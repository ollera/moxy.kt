package com.ollera.moxy.processor.viewstateprovider

import com.ollera.moxy.mvp.DefaultView
import com.ollera.moxy.mvp.DefaultViewState
import com.ollera.moxy.mvp.InjectViewState
import com.ollera.moxy.mvp.MvpPresenter
import com.ollera.moxy.mvp.MvpProcessor.Companion.VIEW_STATE_SUFFIX
import com.ollera.moxy.processor.ElementProcessor
import com.ollera.moxy.processor.MvpProcessor
import com.ollera.moxy.processor.Util
import com.ollera.moxy.processor.Util.fillGenerics
import com.ollera.moxy.processor.Util.getFullClassName
import java.util.*
import javax.lang.model.element.TypeElement
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.MirroredTypeException
import javax.lang.model.type.TypeKind
import javax.lang.model.type.TypeMirror

class InjectViewStateProcessor : ElementProcessor<TypeElement, PresenterInfo>() {

    val usedViews = HashSet<TypeElement>()
    val presenterClassNames = mutableListOf<TypeElement>()

    override fun process(element: TypeElement): PresenterInfo {
        presenterClassNames.add(element)
        return PresenterInfo(element, getViewStateClassName(element)!!)
    }

    private fun getViewStateClassName(typeElement: TypeElement): String? {
        var viewState = getViewStateClassFromAnnotationParams(typeElement)
        if (viewState == null) {
            var view = getViewClassFromAnnotationParams(typeElement)
            if (view == null) {
                view = getViewClassFromGeneric(typeElement)
            }
            if (view.contains("<")) {
                view = view.substring(0, view.indexOf("<"))
            }
            val viewTypeElement = MvpProcessor.elementUtils.getTypeElement(view)
                    ?: throw IllegalArgumentException("View \"$view\" for $typeElement cannot be found")
            usedViews.add(viewTypeElement)
            viewState = getFullClassName(viewTypeElement) + VIEW_STATE_SUFFIX
        }
        return viewState
    }

    private fun getViewClassFromAnnotationParams(typeElement: TypeElement): String? {
        val annotation = typeElement.getAnnotation(InjectViewState::class.java)
        var mvpViewClassName = ""
        if (annotation != null) {
            var value: TypeMirror? = null
            try {
                annotation.view
            } catch (mte: MirroredTypeException) {
                value = mte.typeMirror
            }
            mvpViewClassName = getFullClassName(value!!)
        }
        return if (mvpViewClassName.isEmpty() || DefaultView::class.java.name == mvpViewClassName) {
            null
        } else mvpViewClassName
    }

    private fun getViewStateClassFromAnnotationParams(typeElement: TypeElement): String? {
        val annotation = typeElement.getAnnotation(InjectViewState::class.java)
        var mvpViewStateClassName = ""
        if (annotation != null) {
            val value: TypeMirror
            try {
                annotation.value
            } catch (mte: MirroredTypeException) {
                value = mte.typeMirror
                mvpViewStateClassName = value.toString()
            }
        }
        return if (mvpViewStateClassName.isEmpty() || DefaultViewState::class.java.name == mvpViewStateClassName) {
            null
        } else mvpViewStateClassName
    }

    private fun getViewClassFromGeneric(typeElement: TypeElement): String {
        var superclass = typeElement.asType()
        var parentTypes = emptyMap<String, String>()
        while (superclass.kind != TypeKind.NONE) {
            val superclassElement = (superclass as DeclaredType).asElement() as TypeElement
            val typeArguments = superclass.typeArguments
            val typeParameters = superclassElement.typeParameters
            if (typeArguments.size > typeParameters.size) {
                throw IllegalArgumentException("Code generation for interface " + typeElement.simpleName + " failed. Simplify your generics. (" + typeArguments + " vs " + typeParameters + ")")
            }
            val types = HashMap<String, String>()
            for (i in typeArguments.indices) {
                types[typeParameters[i].toString()] = fillGenerics(parentTypes, typeArguments[i])
            }
            if (superclassElement.toString() == MVP_PRESENTER_CLASS) {
                return fillGenerics(parentTypes, typeArguments)
            }
            parentTypes = types
            superclass = superclassElement.superclass
        }
        return ""
    }

    companion object {
        private val MVP_PRESENTER_CLASS = MvpPresenter::class.java.canonicalName
    }
}
