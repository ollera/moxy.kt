package com.ollera.moxy.processor.presenterbinder

import com.ollera.moxy.mvp.MvpPresenter
import com.ollera.moxy.mvp.MvpProcessor.Companion.PRESENTER_BINDER_SUFFIX
import com.ollera.moxy.mvp.PresenterBinder
import com.ollera.moxy.mvp.presenter.PresenterField
import com.ollera.moxy.processor.FileSpecsGenerator
import com.ollera.moxy.processor.Util.hasEmptyConstructor
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import javax.lang.model.element.TypeElement
import javax.lang.model.type.DeclaredType

internal class PresenterBinderClassGenerator : FileSpecsGenerator<TargetClassInfo>() {

    override fun generate(input: TargetClassInfo): List<FileSpec> {
        val targetClassName = input.name
        val fields = input.fields
        val containerSimpleName = targetClassName.simpleNames.joinToString("$")
        val classBuilder = TypeSpec.classBuilder(containerSimpleName + PRESENTER_BINDER_SUFFIX)
                .addModifiers(KModifier.PUBLIC)
                .superclass(PresenterBinder::class.asClassName().parameterizedBy(targetClassName))
        for (field in fields) {
            classBuilder.addType(generatePresenterBinderClass(field, targetClassName))
        }
        classBuilder.addProperty(generateGetPresentersMethod(fields, targetClassName))
        val javaFile = FileSpec.builder(targetClassName.packageName, targetClassName.simpleName)
                .addType(classBuilder.build())
                .indent("\t")
                .build()
        return listOf(javaFile)
    }

    private fun generateGetPresentersMethod(fields: List<TargetPresenterField>, containerClassName: ClassName): PropertySpec {
        val type = List::class.asClassName().parameterizedBy(PresenterField::class.asClassName().parameterizedBy(containerClassName))
        val builder = PropertySpec.builder("presenterFields", type, KModifier.OVERRIDE, KModifier.PUBLIC)
        val getterFunSpecBuilder = FunSpec.builder("get()")
                .addStatement("val presenters: MutableList<${PresenterField::class.qualifiedName}<$containerClassName>> = mutableListOf()")
        for (field in fields) {
            getterFunSpecBuilder.addStatement("presenters.add(${field.generatedClassName}())")
        }
        getterFunSpecBuilder.addStatement("return presenters")
        val getterFunSpec = getterFunSpecBuilder.build()
        builder.getter(getterFunSpec)
        return builder.build()
    }

    private fun generatePresenterBinderClass(field: TargetPresenterField, targetClassName: ClassName): TypeSpec {
        val tag = if (field.tag.isEmpty()) "\"\"" else field.tag
        val presenterId = if (field.presenterId.isEmpty()) "\"\"" else field.presenterId
        val classBuilder = TypeSpec.classBuilder(field.generatedClassName)
                .addModifiers(KModifier.PUBLIC)
                .superclass(PresenterField::class.asClassName().parameterizedBy(targetClassName))
                .addSuperclassConstructorParameter(tag)
                .addSuperclassConstructorParameter("${field.presenterType.declaringClass.canonicalName}.${field.presenterType.name}")
                .addSuperclassConstructorParameter(presenterId)
                .addSuperclassConstructorParameter("${field.typeName}::class")
                .addFunction(generateBindMethod(field, targetClassName))
                .addFunction(generateProvidePresenterMethod(field, targetClassName))

        val tagProviderMethodName = field.presenterTagProviderMethodName
        if (tagProviderMethodName != null) {
            classBuilder.addFunction(generateGetTagMethod(tagProviderMethodName, targetClassName))
        }
        return classBuilder.build()
    }

    private fun generateBindMethod(field: TargetPresenterField, targetClassName: ClassName): FunSpec {
        return FunSpec.builder("bind")
                .addModifiers(KModifier.OVERRIDE)
                .addModifiers(KModifier.PUBLIC)
                .addParameter("container", targetClassName)
                .addParameter("presenter", MvpPresenter::class.asClassName().parameterizedBy(STAR))
                .addStatement("container.${field.name} = presenter as ${field.typeName}")
                .build()
    }

    private fun generateProvidePresenterMethod(field: TargetPresenterField, targetClassName: ClassName): FunSpec {
        val builder = FunSpec.builder("providePresenter")
                .addModifiers(KModifier.OVERRIDE)
                .addModifiers(KModifier.PUBLIC)
                .returns(MvpPresenter::class.asClassName().parameterizedBy(STAR))
                .addParameter("delegated", targetClassName)

        val presenterProviderMethodName = field.presenterProviderMethodName
        if (presenterProviderMethodName != null) {
            builder.addStatement("return delegated.$presenterProviderMethodName()")
        } else {
            val hasEmptyConstructor = hasEmptyConstructor((field.clazz as DeclaredType).asElement() as TypeElement)

            if (hasEmptyConstructor) {
                builder.addStatement("return ${field.typeName}()")
            } else {
                builder.addStatement("throw ${IllegalStateException::class.qualifiedName}(${field.clazz} + has not default constructor. You can apply @ProvidePresenter to some method which will construct Presenter. Also you can make it default constructor)")
            }
        }
        return builder.build()
    }

    private fun generateGetTagMethod(tagProviderMethodName: String, targetClassName: ClassName): FunSpec {
        return FunSpec.builder("getTag")
                .addModifiers(KModifier.OVERRIDE)
                .addModifiers(KModifier.PUBLIC)
                .returns(String::class)
                .addParameter("delegated", targetClassName)
                .addStatement("return String.valueOf(delegated.\$L())", tagProviderMethodName)
                .build()
    }
}
