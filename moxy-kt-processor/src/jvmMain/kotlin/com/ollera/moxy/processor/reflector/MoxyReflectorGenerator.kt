package com.ollera.moxy.processor.reflector

import com.ollera.moxy.mvp.MvpProcessor.Companion.PRESENTER_BINDER_SUFFIX
import com.ollera.moxy.mvp.MvpProcessor.Companion.VIEW_STATE_PROVIDER_SUFFIX
import com.ollera.moxy.processor.MvpProcessor.Companion.MOXY_REFLECTOR_DEFAULT_PACKAGE
import com.ollera.moxy.processor.ViewStateProvider
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy
import java.util.*
import javax.lang.model.element.TypeElement
import javax.lang.model.type.DeclaredType
import javax.lang.model.type.TypeKind
import kotlin.reflect.KClass

object MoxyReflectorGenerator {

    private val CLASS_WILDCARD_TYPE_NAME = KClass::class.asClassName().parameterizedBy(WildcardTypeName.producerOf(ANY))
    private val LIST_OF_OBJECT_TYPE_NAME = List::class.asClassName().parameterizedBy(ANY)
    private val MAP_CLASS_TO_OBJECT_TYPE_NAME = MutableMap::class.asClassName().parameterizedBy(CLASS_WILDCARD_TYPE_NAME, ANY)
    private val MAP_CLASS_TO_LIST_OF_OBJECT_TYPE_NAME = MutableMap::class.asClassName().parameterizedBy(CLASS_WILDCARD_TYPE_NAME, LIST_OF_OBJECT_TYPE_NAME)

    fun generate(destinationPackage: String,
                 presenterClassNames: List<TypeElement>,
                 presentersContainers: List<TypeElement>,
                 strategyClasses: List<TypeElement>,
                 additionalMoxyReflectorsPackages: List<String>): FileSpec {
        val objectBuilder = TypeSpec.objectBuilder("MoxyReflector")
                .addModifiers(KModifier.PUBLIC)
                .addProperty("sViewStateProviders", MAP_CLASS_TO_OBJECT_TYPE_NAME, KModifier.PRIVATE)
                .addProperty("sPresenterBinders", MAP_CLASS_TO_LIST_OF_OBJECT_TYPE_NAME, KModifier.PRIVATE)
                .addProperty("sStrategies", MAP_CLASS_TO_OBJECT_TYPE_NAME, KModifier.PRIVATE)
                .addInitializerBlock(generateStaticInitializer(presenterClassNames, presentersContainers, strategyClasses, additionalMoxyReflectorsPackages))
        if (destinationPackage == MOXY_REFLECTOR_DEFAULT_PACKAGE) {
            objectBuilder.addFunction(FunSpec.builder("getViewState")
                    .addModifiers(KModifier.PUBLIC)
                    .returns(Any::class.asTypeName().copy(nullable = true))
                    .addParameter("presenterClass", CLASS_WILDCARD_TYPE_NAME)
                    .addStatement("val viewStateProvider: %1T?  = sViewStateProviders.getValue(presenterClass) as %1T? ?: return null", ViewStateProvider::class)
                    .addCode("\n")
                    .addStatement("return viewStateProvider?.viewState")
                    .build())
            objectBuilder.addFunction(FunSpec.builder("getPresenterBinders")
                    .addModifiers(KModifier.PUBLIC)
                    .returns(List::class.asClassName().parameterizedBy(Any::class.asTypeName()))
                    .addParameter("delegated", CLASS_WILDCARD_TYPE_NAME)
                    .addStatement("return sPresenterBinders.getValue(delegated)")
                    .build())
            objectBuilder.addFunction(FunSpec.builder("getStrategy")
                    .addModifiers(KModifier.PUBLIC)
                    .returns(Any::class)
                    .addParameter("strategyClass", CLASS_WILDCARD_TYPE_NAME)
                    .addStatement("return sStrategies.getValue(strategyClass)")
                    .build())
        } else {
            objectBuilder.addFunction(FunSpec.builder("getViewStateProviders")
                    .addModifiers(KModifier.PUBLIC)
                    .returns(MAP_CLASS_TO_OBJECT_TYPE_NAME)
                    .addStatement("return sViewStateProviders")
                    .build())
            objectBuilder.addFunction(FunSpec.builder("getPresenterBinders")
                    .addModifiers(KModifier.PUBLIC)
                    .returns(MAP_CLASS_TO_LIST_OF_OBJECT_TYPE_NAME)
                    .addStatement("return sPresenterBinders")
                    .build())
            objectBuilder.addFunction(FunSpec.builder("getStrategies")
                    .addModifiers(KModifier.PUBLIC)
                    .returns(MAP_CLASS_TO_OBJECT_TYPE_NAME)
                    .addStatement("return sStrategies")
                    .build())
        }

        val typeSpec = objectBuilder.build()
        return FileSpec.builder(destinationPackage, typeSpec.name!!)
                .addType(typeSpec)
                .indent("\t")
                .build()
    }

    private fun generateStaticInitializer(presenterClassNames: List<TypeElement>,
                                          presentersContainers: List<TypeElement>,
                                          strategyClasses: List<TypeElement>,
                                          additionalMoxyReflectorsPackages: List<String>): CodeBlock {
        // sort to preserve order of statements between compilations
        val presenterBinders = getPresenterBinders(presentersContainers)
        presenterClassNames.sortedBy { it.toString() }
        strategyClasses.sortedBy { it.toString() }
        additionalMoxyReflectorsPackages.sorted()
        val builder = CodeBlock.builder()
        builder.addStatement("sViewStateProviders = %T()", HashMap::class)
        for (presenter in presenterClassNames) {
            val presenterClassName = presenter.asClassName()
            val viewStateProvider = ClassName(presenterClassName.packageName, presenterClassName.simpleNames.joinToString("_") + VIEW_STATE_PROVIDER_SUFFIX)
            builder.addStatement("sViewStateProviders.put(%T::class, %T())", presenterClassName, viewStateProvider)
        }
        builder.add("\n")
        builder.addStatement("sPresenterBinders = %T()", HashMap::class)
        for ((key, value) in presenterBinders) {
            builder.add("sPresenterBinders.put(%T::class, listOf(", key)
            var isFirst = true
            for (typeElement in value) {
                val className = typeElement.asClassName()
                val presenterBinderName = className.simpleNames.joinToString("_") + PRESENTER_BINDER_SUFFIX

                if (isFirst) {
                    isFirst = false
                } else {
                    builder.add(", ")
                }
                builder.add("%T()", ClassName(className.packageName, presenterBinderName))
            }
            builder.add("))\n")
        }
        builder.add("\n")
        builder.addStatement("sStrategies = %T()", HashMap::class.java)
        for (strategyClass in strategyClasses) {
            builder.addStatement("sStrategies.put(%T::class, %T())", strategyClass)
        }
        for (pkg in additionalMoxyReflectorsPackages) {
            val moxyReflector = ClassName(pkg, "MoxyReflector")
            builder.add("\n")
            builder.addStatement("sViewStateProviders.putAll(%T.getViewStateProviders())", moxyReflector)
            builder.addStatement("sPresenterBinders.putAll(%T.getPresenterBinders())", moxyReflector)
            builder.addStatement("sStrategies.putAll(%T.getStrategies())", moxyReflector)
        }
        return builder.build()
    }

    /**
     * Collects presenter binders from superclasses that are also presenter containers.
     *
     * @return sorted map between presenter container and list of corresponding binders
     */
    private fun getPresenterBinders(presentersContainers: List<TypeElement>): SortedMap<TypeElement, List<TypeElement>> {
        val extendingMap = mutableMapOf<TypeElement, TypeElement>()
        for (presentersContainer in presentersContainers) {
            var superclass = presentersContainer.superclass
            var parent: TypeElement? = null
            while (superclass.kind == TypeKind.DECLARED) {
                val superclassElement = (superclass as DeclaredType).asElement() as TypeElement
                if (presentersContainers.contains(superclassElement)) {
                    parent = superclassElement
                    break
                }
                superclass = superclassElement.superclass
            }
            parent?.let {
                extendingMap[presentersContainer] = parent
            }
        }
        val elementListMap = TreeMap<TypeElement, List<TypeElement>>(compareBy { it.toString() })
        for (presentersContainer in presentersContainers) {
            val typeElements = ArrayList<TypeElement>()
            typeElements.add(presentersContainer)
            var key: TypeElement? = presentersContainer
            while (key != null) {
                typeElements.add(key)
                key = extendingMap[key]
            }
            elementListMap[presentersContainer] = typeElements
        }
        return elementListMap
    }
}