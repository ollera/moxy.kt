package com.ollera.moxy.processor.presenterbinder

import com.ollera.moxy.mvp.presenter.PresenterType
import javax.lang.model.type.TypeMirror

internal class TagProviderMethod(val presenterClass: TypeMirror?, val methodName: String, type: String, val presenterId: String) {
    val type: PresenterType = PresenterType.valueOf(type)
}
