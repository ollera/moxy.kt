package com.ollera.moxy.processor

import com.google.auto.service.AutoService
import com.ollera.moxy.mvp.InjectViewState
import com.ollera.moxy.mvp.RegisterMoxyReflectorPackages
import com.ollera.moxy.mvp.presenter.InjectPresenter
import com.ollera.moxy.processor.presenterbinder.InjectPresenterProcessor
import com.ollera.moxy.processor.presenterbinder.PresenterBinderClassGenerator
import com.ollera.moxy.processor.reflector.MoxyReflectorGenerator.generate
import com.ollera.moxy.processor.viewstate.ViewInterfaceProcessor
import com.ollera.moxy.processor.viewstate.ViewStateClassGenerator
import com.ollera.moxy.processor.viewstateprovider.InjectViewStateProcessor
import com.ollera.moxy.processor.viewstateprovider.ViewStateProviderClassGenerator
import com.squareup.kotlinpoet.FileSpec
import java.io.IOException
import java.util.*
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.Modifier
import javax.lang.model.element.TypeElement
import javax.lang.model.util.Elements
import javax.lang.model.util.Types
import javax.tools.Diagnostic

@AutoService(Processor::class)
class MvpProcessor : AbstractProcessor() {

    @Synchronized
    override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)
        messager = processingEnv.messager
        typeUtils = processingEnv.typeUtils
        elementUtils = processingEnv.elementUtils
        sOptions = processingEnv.options
    }

    override fun getSupportedOptions(): Set<String> {
        return setOf(OPTION_MOXY_REFLECTOR_PACKAGE)
    }

    override fun getSupportedAnnotationTypes(): Set<String> {
        val supportedAnnotationTypes = HashSet<String>()
        Collections.addAll(supportedAnnotationTypes,
                InjectPresenter::class.java.canonicalName,
                InjectViewState::class.java.canonicalName,
                RegisterMoxyReflectorPackages::class.java.canonicalName)
        return supportedAnnotationTypes
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

    override fun process(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Boolean {
        if (annotations.isEmpty()) {
            return false
        }
        try {
            return throwableProcess(roundEnv)
        } catch (e: RuntimeException) {
            messager.printMessage(Diagnostic.Kind.OTHER, "Moxy compilation failed. Could you copy stack trace above and write us (or make issue on Github)?")
            e.printStackTrace()
            messager.printMessage(Diagnostic.Kind.ERROR, "Moxy compilation failed; see the compiler error output for details ($e)")
        }
        return true
    }

    private fun throwableProcess(roundEnv: RoundEnvironment): Boolean {
        checkInjectors(roundEnv, InjectPresenter::class.java, PresenterInjectorRules(ElementKind.FIELD, Modifier.PUBLIC, Modifier.DEFAULT))

        val injectViewStateProcessor = InjectViewStateProcessor()
        val viewStateProviderClassGenerator = ViewStateProviderClassGenerator()

        val injectPresenterProcessor = InjectPresenterProcessor()
        val presenterBinderClassGenerator = PresenterBinderClassGenerator()

        val viewInterfaceProcessor = ViewInterfaceProcessor()
        val viewStateClassGenerator = ViewStateClassGenerator()

        processInjectors(roundEnv, InjectViewState::class.java, ElementKind.CLASS, injectViewStateProcessor, viewStateProviderClassGenerator)
        processInjectors(roundEnv, InjectPresenter::class.java, ElementKind.FIELD, injectPresenterProcessor, presenterBinderClassGenerator)

        for (usedView in injectViewStateProcessor.usedViews) {
            generateCode(usedView, ElementKind.INTERFACE, viewInterfaceProcessor, viewStateClassGenerator)
        }

        val moxyReflectorPackage: String = sOptions[OPTION_MOXY_REFLECTOR_PACKAGE]?:MOXY_REFLECTOR_DEFAULT_PACKAGE
        val additionalMoxyReflectorPackages = getAdditionalMoxyReflectorPackages(roundEnv)
        val moxyReflector = generate(
                moxyReflectorPackage,
                injectViewStateProcessor.presenterClassNames,
                injectPresenterProcessor.presentersContainers,
                viewInterfaceProcessor.getUsedStrategies(),
                additionalMoxyReflectorPackages
        )
        createSourceFile(moxyReflector)
        return true
    }

    private fun getAdditionalMoxyReflectorPackages(roundEnv: RoundEnvironment): List<String> {
        val result = ArrayList<String>()
        for (element in roundEnv.getElementsAnnotatedWith(RegisterMoxyReflectorPackages::class.java)) {
            if (element.kind != ElementKind.CLASS) {
                messager.printMessage(Diagnostic.Kind.ERROR, element.toString() + " must be " + ElementKind.CLASS.name + ", or not mark it as @" + RegisterMoxyReflectorPackages::class.java.simpleName)
            }
            val packages = element.getAnnotation(RegisterMoxyReflectorPackages::class.java).value
            Collections.addAll(result, *packages)
        }
        return result
    }

    private fun checkInjectors(roundEnv: RoundEnvironment, clazz: Class<out Annotation>, annotationRule: AnnotationRule) {
        for (annotatedElement in roundEnv.getElementsAnnotatedWith(clazz)) {
            annotationRule.checkAnnotation(annotatedElement)
        }
        val errorStack = annotationRule.errorStack
        if (errorStack.isNotEmpty()) {
            messager.printMessage(Diagnostic.Kind.ERROR, errorStack)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun <E : Element, R> processInjectors(roundEnv: RoundEnvironment,
                                                  clazz: Class<out Annotation>,
                                                  kind: ElementKind,
                                                  processor: ElementProcessor<E, R>,
                                                  classGenerator: FileSpecsGenerator<R>) {
        for (element in roundEnv.getElementsAnnotatedWith(clazz)) {
            if (element.kind != kind) {
                messager.printMessage(Diagnostic.Kind.ERROR,
                        element.toString() + " must be " + kind.name + ", or not mark it as @" + clazz.simpleName)
            }
            generateCode(element as E, kind, processor, classGenerator)
        }
    }

    private fun <E : Element, R> generateCode(element: E,
                                              kind: ElementKind,
                                              processor: ElementProcessor<E, R>,
                                              classGenerator: FileSpecsGenerator<R>) {
        if (element.kind != kind) {
            messager.printMessage(Diagnostic.Kind.ERROR, element.toString() + " must be " + kind.name)
        }
        val result = processor.process(element) ?: return
        for (file in classGenerator.generate(result)) {
            createSourceFile(file)
        }
    }

    private fun createSourceFile(file: FileSpec) {
        try {
            file.writeTo(processingEnv.filer)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    companion object {
        const val MOXY_REFLECTOR_DEFAULT_PACKAGE = "com.ollera.mvp"
        private const val OPTION_MOXY_REFLECTOR_PACKAGE = "moxyReflectorPackage"
        lateinit var messager: Messager
            private set
        lateinit var typeUtils: Types
            private set
        lateinit var elementUtils: Elements
            private set
        private lateinit var sOptions: Map<String, String>
    }
}