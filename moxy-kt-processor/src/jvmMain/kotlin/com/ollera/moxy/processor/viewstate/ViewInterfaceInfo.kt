package com.ollera.moxy.processor.viewstate

import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy

import javax.lang.model.element.TypeElement

class ViewInterfaceInfo(val element: TypeElement, val methods: List<ViewMethod>) {
    val name: ClassName = element.asClassName()
    val typeVariables: List<TypeVariableName> = element.typeParameters.map { it.asTypeVariableName() }
    val nameWithTypeVariables: TypeName
        get() {
            return if (typeVariables.isEmpty()) {
                name
            } else {
                val names = typeVariables.toTypedArray()
                name.parameterizedBy(*names)
            }
        }
}
