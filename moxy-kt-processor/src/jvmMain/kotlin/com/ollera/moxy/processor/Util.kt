package com.ollera.moxy.processor

import javax.lang.model.element.*
import javax.lang.model.type.*

object Util {

    fun getFullClassName(typeMirror: TypeMirror): String {
        if (typeMirror !is DeclaredType) {
            return ""
        }

        val typeElement = typeMirror.asElement() as TypeElement
        return getFullClassName(typeElement)
    }

    fun getFullClassName(typeElement: TypeElement): String {
        var packageName = MvpProcessor.elementUtils.getPackageOf(typeElement).qualifiedName.toString()
        if (packageName.isNotEmpty()) {
            packageName += "."
        }

        val className = typeElement.toString().substring(packageName.length)
        return packageName + className.replace("\\.".toRegex(), "\\$")
    }

    fun fillGenerics(types: Map<String, String>, param: TypeMirror): String {
        return fillGenerics(types, listOf(param))
    }

    fun fillGenerics(types: Map<String, String>, params: List<TypeMirror>, separator: String = ", "): String {
        var result = ""

        for (param in params) {
            if (result.isNotEmpty()) {
                result += separator
            }

            /**
             * "if" block's order is critically! E.g. IntersectionType is TypeVariable.
             */
            if (param is WildcardType) {
                result += "?"
                val extendsBound = param.extendsBound
                if (extendsBound != null) {
                    result += " extends " + fillGenerics(types, extendsBound)
                }
                val superBound = param.superBound
                if (superBound != null) {
                    result += " super " + fillGenerics(types, superBound)
                }
            } else if (param is IntersectionType) {
                result += "?"
                val bounds = param.bounds

                if (bounds.isNotEmpty()) {
                    result += " extends " + fillGenerics(types, bounds, " & ")
                }
            } else if (param is DeclaredType) {
                result += param.asElement()

                val typeArguments = param.typeArguments
                if (typeArguments.isNotEmpty()) {
                    val s = fillGenerics(types, typeArguments)

                    result += "<$s>"
                }
            } else if (param is TypeVariable) {
                var type: String? = types[param.toString()]
                if (type == null) {
                    type = param.toString()
                }
                result += type
            } else {
                result += param
            }
        }
        return result
    }

    fun getAnnotation(element: Element, annotationClass: String): AnnotationMirror? {
        for (annotationMirror in element.annotationMirrors) {
            if (annotationMirror.annotationType.asElement().toString() == annotationClass)
                return annotationMirror
        }
        return null
    }

    fun getAnnotationValueAsTypeMirror(annotationMirror: AnnotationMirror?, key: String): TypeMirror? {
        val av = getAnnotationValue(annotationMirror, key)
        return if (av != null) {
            av.value as TypeMirror
        } else {
            null
        }
    }

    fun getAnnotationValueAsString(annotationMirror: AnnotationMirror?, key: String): String? {
        return getAnnotationValue(annotationMirror, key)?.value?.toString()
    }

    fun decapitalizeString(string: String?): String {
        return if (string == null || string.isEmpty()) "" else if (string.length == 1) string.toLowerCase() else Character.toLowerCase(string[0]) + string.substring(1)
    }

    fun hasEmptyConstructor(element: TypeElement): Boolean {
        for (enclosedElement in element.enclosedElements) {
            if (enclosedElement.kind == ElementKind.CONSTRUCTOR) {
                val parameters = (enclosedElement as ExecutableElement).parameters
                if (parameters == null || parameters.isEmpty()) {
                    return true
                }
            }
        }
        return false
    }

    private fun getAnnotationValue(annotationMirror: AnnotationMirror?, key: String): AnnotationValue? {
        if (annotationMirror == null) return null
        for ((key1, value) in annotationMirror.elementValues) {
            if (key1.simpleName.toString() == key) {
                return value
            }
        }
        return null
    }
}