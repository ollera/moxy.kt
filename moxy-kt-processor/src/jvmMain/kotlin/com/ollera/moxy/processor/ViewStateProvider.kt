package com.ollera.moxy.processor

import com.ollera.moxy.mvp.viewstate.MvpViewState

abstract class ViewStateProvider {
    /**
     *
     * Presenter creates view state object by calling this method.
     *
     * @return view state class name
     */
    abstract val viewState: MvpViewState<*>
}