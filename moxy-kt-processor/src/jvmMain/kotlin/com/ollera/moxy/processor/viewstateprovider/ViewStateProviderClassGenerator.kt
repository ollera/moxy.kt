package com.ollera.moxy.processor.viewstateprovider

import com.ollera.moxy.mvp.MvpProcessor.Companion.VIEW_STATE_PROVIDER_SUFFIX
import com.ollera.moxy.mvp.MvpView
import com.ollera.moxy.mvp.viewstate.MvpViewState
import com.ollera.moxy.processor.FileSpecsGenerator
import com.ollera.moxy.processor.ViewStateProvider
import com.squareup.kotlinpoet.*
import com.squareup.kotlinpoet.ParameterizedTypeName.Companion.parameterizedBy

class ViewStateProviderClassGenerator : FileSpecsGenerator<PresenterInfo>() {

    override fun generate(input: PresenterInfo): List<FileSpec> {
        val typeSpec = TypeSpec.classBuilder(input.name.simpleName + VIEW_STATE_PROVIDER_SUFFIX)
                .addModifiers(KModifier.PUBLIC)
                .superclass(ViewStateProvider::class.java)
                .addProperty(generateGetViewStateMethod(input.name, input.viewStateName))
                .build()

        val javaFile = FileSpec.builder(input.name.packageName, input.name.simpleName)
                .addType(typeSpec)
                .indent("\t")
                .build()

        return listOf(javaFile)
    }


    private fun generateGetViewStateMethod(presenter: ClassName, viewState: ClassName?): PropertySpec {
        val valueBuilder = PropertySpec.builder("viewState", MvpViewState::class.asClassName().parameterizedBy(WildcardTypeName.producerOf(MvpView::class)))
                .addModifiers(KModifier.PUBLIC, KModifier.OVERRIDE)
                .mutable(false)
        if (viewState == null) {
            valueBuilder.getter(FunSpec.builder("get()")
                    .addStatement("throw new RuntimeException(%S)", presenter.reflectionName() + " should has view")
                    .build())
        } else {
            valueBuilder.getter(FunSpec.builder("get()")
                    .addStatement("return %T()", viewState)
                    .build())
        }
        return valueBuilder.build()
    }
}