package com.ollera.moxy.processor.presenterbinder

import com.squareup.kotlinpoet.ClassName
import javax.lang.model.element.TypeElement

class TargetClassInfo(name: TypeElement, val fields: List<TargetPresenterField>) {
    val name: ClassName = ClassName.Companion.bestGuess(name.qualifiedName.toString())
}
