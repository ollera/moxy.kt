package com.ollera.moxy.processor

import javax.lang.model.element.Element

abstract class ElementProcessor<E : Element, R> {
    abstract fun process(element: E): R?
}