package com.ollera.moxy.processor.viewstateprovider

import com.squareup.kotlinpoet.ClassName
import com.squareup.kotlinpoet.asClassName
import javax.lang.model.element.TypeElement

class PresenterInfo(name: TypeElement, viewStateName: String) {
    val name: ClassName = name.asClassName()
    val viewStateName: ClassName = ClassName.bestGuess(viewStateName)
}