import org.gradle.api.internal.artifacts.dependencies.DefaultExternalModuleDependency
import org.gradle.internal.jvm.Jvm

plugins {
    kotlin("multiplatform")
    kotlin("kapt")
}

kotlin {
    jvm()
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
            }
        }
        val jvmMain by getting {
            dependsOn(commonMain)
            dependencies {
                implementation(project(":moxy-kt", "jvmDefault"))
                implementation(kotlin("stdlib-jdk8"))
                implementation("com.squareup:kotlinpoet:1.3.0")
                implementation("com.google.auto.service:auto-service:1.0-rc6")
                configurations["kapt"].dependencies.add(DefaultExternalModuleDependency("com.google.auto.service", "auto-service", "1.0-rc6"))
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation("org.junit.jupiter:junit-jupiter-api:5.1.0")
                runtimeOnly("org.junit.jupiter:junit-jupiter-engine:5.1.0")
                implementation("io.kotlintest:kotlintest-runner-junit5:3.4.0")
                implementation("com.github.tschuchortdev:kotlin-compile-testing:1.1.2")
                implementation(files(Jvm.current().toolsJar))
            }
        }
    }
}

tasks.named<Test>("jvmTest") {
    useJUnitPlatform()
}