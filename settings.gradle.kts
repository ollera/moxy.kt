val kotlinVersion: String by settings
pluginManagement {
    plugins {
        kotlin("multiplatform") version kotlinVersion
        kotlin("jvm") version kotlinVersion
        kotlin("kapt") version  kotlinVersion
    }
    repositories {
        jcenter()
        mavenCentral()
        google()
        maven(url = "https://jitpack.io")
        gradlePluginPortal()
    }
}

rootProject.name = "moxy.kt"

include(":moxy-kt",
        ":moxy-kt-processor",
        ":moxy-kt-android-sample")
