group = "ollera"
version = "1.0.0-alpha01"


buildscript {
    val kotlinVersion: String = properties["kotlinVersion"] as String
    val androidPluginVersion: String = properties["androidPluginVersion"] as String

    repositories {
        jcenter()
        mavenCentral()
        google()
        maven(url = "https://jitpack.io")
    }

    dependencies {
        classpath("com.android.tools.build:gradle:$androidPluginVersion")
        classpath(kotlin("gradle-plugin", version = kotlinVersion))
    }
}

allprojects {
    repositories {
        jcenter()
        mavenCentral()
        google()
        maven(url = "https://jitpack.io")
    }
}